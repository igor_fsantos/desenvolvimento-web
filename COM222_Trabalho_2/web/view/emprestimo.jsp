<%-- 
    Document   : emprestimo
    Created on : Oct 9, 2016, 3:59:24 AM
    Author     : Igor Santos
--%>

<%@ include file="../header/validarSessao.jsp"%>
<%@ include file="../header/h_principalAdmin.jsp"%>
<h1 class="page-header">Empr�stimo</h1>
<form method="post" action="../validarEmprestimo">
    <div class="form-group row">
        <label for="numero" class="col-xs-2 col-form-label">N�mero do exemplar</label>
        <div class="col-xs-10">
            <input class="form-control" type="number" name="number" id="number" min="1" max="9999999999999" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="isbn" class="col-xs-2 col-form-label">ISBN</label>
        <div class="col-xs-10">
            <input class="form-control" type="number" name="isbn" id="isbn" min="1" max="9999999999999" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="data" class="col-xs-2 col-form-label">Data do empr�stimo</label>
        <div class="col-xs-2">
            <input class="form-control" type="number" min="1" max="31" name="dia" id="dia" placeholder="dia" required>
        </div>
        <div class="col-xs-2">
            <input class="form-control" type="number" min="1" max="12" name="mes" id="mes" placeholder="m�s" required>
        </div>
        <div class="col-xs-2">
            <input class="form-control" type="number" min="1900" max="2017" name="ano" id="ano" placeholder="ano" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="codigo" class="col-xs-2 col-form-label">C�digo do associado</label>
        <div class="col-xs-10">
            <input class="form-control" type="number" name="codigo" id="codigo" min="1" max="99999999999999" required>
        </div>
    </div>
    <center>
        <button type="submit" class="btn btn-primary">Emprestar</button>
        <button class="btn btn-danger" type="reset">Reset</button>
    </center>
</form>
<%@ include file="../footer/verificarErro.jsp"%>
<%@ include file="../footer/footer.jsp"%>