-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2016 at 09:15 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biblioteca`
--

-- --------------------------------------------------------

--
-- Table structure for table `associado`
--

CREATE TABLE `associado` (
  `codigo` int(15) UNSIGNED NOT NULL,
  `nome` varchar(20) NOT NULL,
  `sobrenome` varchar(20) NOT NULL,
  `endereco` varchar(120) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(60) NOT NULL,
  `status` enum('grad','posgrad','prof') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `associado`
--

INSERT INTO `associado` (`codigo`, `nome`, `sobrenome`, `endereco`, `email`, `senha`, `status`) VALUES
(30273, 'Phelip', 'Roberto', 'Rua Osvaldo Cruz', 'phelip@hotmail.com', 'teste4', 'prof'),
(30469, 'Daniel', 'Shinkai', 'Rua Osvaldo Cruz', 'daniel@gmail.com', 'teste2', 'grad'),
(31975, 'Patrick', 'Perroni', 'Rua Tertuliano', 'patrick@gmail.com', 'teste3', 'posgrad');

-- --------------------------------------------------------

--
-- Table structure for table `devolucao`
--

CREATE TABLE `devolucao` (
  `cod_emprestimo` int(40) UNSIGNED NOT NULL,
  `codigo_func` int(20) UNSIGNED NOT NULL,
  `prazo_dev` date NOT NULL,
  `data_dev` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `devolucao`
--
DELIMITER $$
CREATE TRIGGER `finalizaEmprestimo` AFTER INSERT ON `devolucao` FOR EACH ROW UPDATE emprestimo
    SET emprestimo.finalizado=true WHERE
    emprestimo.cod_emprestimo=NEW.cod_emprestimo
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `emprestimo`
--

CREATE TABLE `emprestimo` (
  `cod_emprestimo` int(40) UNSIGNED NOT NULL,
  `codigo_assoc` int(20) UNSIGNED NOT NULL,
  `codigo_func` int(20) UNSIGNED NOT NULL,
  `num_exemplar` int(15) UNSIGNED NOT NULL,
  `ISBN_exemplar` int(13) UNSIGNED NOT NULL,
  `data_emp` date NOT NULL,
  `prazo_dev` date NOT NULL,
  `finalizado` enum('s','n') NOT NULL DEFAULT 'n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emprestimo`
--

INSERT INTO `emprestimo` (`cod_emprestimo`, `codigo_assoc`, `codigo_func`, `num_exemplar`, `ISBN_exemplar`, `data_emp`, `prazo_dev`, `finalizado`) VALUES
(1, 30469, 31101, 2, 1, '2016-10-10', '2016-10-17', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exemplar`
--

CREATE TABLE `exemplar` (
  `numero` int(15) UNSIGNED NOT NULL,
  `ISBN` int(13) UNSIGNED NOT NULL,
  `preco` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exemplar`
--

INSERT INTO `exemplar` (`numero`, `ISBN`, `preco`) VALUES
(1, 1, 10.5),
(1, 2, 15.7),
(1, 3, 10),
(1, 4, 20),
(1, 5, 50),
(2, 1, 10.5),
(2, 2, 15.7),
(2, 3, 10),
(2, 4, 20),
(2, 5, 50),
(3, 1, 10.5),
(3, 2, 15.7),
(3, 5, 50);

-- --------------------------------------------------------

--
-- Table structure for table `funcionario`
--

CREATE TABLE `funcionario` (
  `codigo` int(15) UNSIGNED NOT NULL,
  `nome` varchar(20) NOT NULL,
  `sobrenome` varchar(20) NOT NULL,
  `endereco` varchar(120) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `funcionario`
--

INSERT INTO `funcionario` (`codigo`, `nome`, `sobrenome`, `endereco`, `email`, `senha`) VALUES
(31101, 'Igor', 'Santos', 'Rua Osvaldo Cruz', 'igorfelipefs@gmail.com', 'teste1');

-- --------------------------------------------------------

--
-- Table structure for table `multa`
--

CREATE TABLE `multa` (
  `cod_emprestimo` int(40) UNSIGNED NOT NULL,
  `valor` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `publicacao`
--

CREATE TABLE `publicacao` (
  `ISBN` int(13) UNSIGNED NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `autor` varchar(120) NOT NULL,
  `editora` varchar(70) NOT NULL,
  `ano` int(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publicacao`
--

INSERT INTO `publicacao` (`ISBN`, `titulo`, `autor`, `editora`, `ano`) VALUES
(1, 'Os Três Porquinhos', 'Joseph Jacobs', 'Todolivro', 1853),
(2, 'Procurando Dory', 'Disney', 'Disney', 2014),
(3, 'Um Leão nos meus Cereais', 'Michelle Robinson', 'Minutos de Leitura', 2013),
(4, 'The Godfather', 'Mario Puzo', 'G. P. Putnam\'s Sons', 1969),
(5, 'The Lord of The Rings', 'J. R. R. Tolkien', 'George Allen & Unwin (UK)', 1955);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `associado`
--
ALTER TABLE `associado`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `devolucao`
--
ALTER TABLE `devolucao`
  ADD PRIMARY KEY (`cod_emprestimo`),
  ADD KEY `codigo_func` (`codigo_func`);

--
-- Indexes for table `emprestimo`
--
ALTER TABLE `emprestimo`
  ADD PRIMARY KEY (`cod_emprestimo`),
  ADD KEY `codigo_assoc` (`codigo_assoc`),
  ADD KEY `codigo_func` (`codigo_func`),
  ADD KEY `num_exemplar` (`num_exemplar`),
  ADD KEY `ISBN_exemplar` (`ISBN_exemplar`);

--
-- Indexes for table `exemplar`
--
ALTER TABLE `exemplar`
  ADD PRIMARY KEY (`numero`,`ISBN`),
  ADD KEY `ISBN` (`ISBN`);

--
-- Indexes for table `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`codigo`);

--
-- Indexes for table `multa`
--
ALTER TABLE `multa`
  ADD PRIMARY KEY (`cod_emprestimo`);

--
-- Indexes for table `publicacao`
--
ALTER TABLE `publicacao`
  ADD PRIMARY KEY (`ISBN`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emprestimo`
--
ALTER TABLE `emprestimo`
  MODIFY `cod_emprestimo` int(40) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `devolucao`
--
ALTER TABLE `devolucao`
  ADD CONSTRAINT `devolucao_ibfk_2` FOREIGN KEY (`codigo_func`) REFERENCES `funcionario` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `devolucao_ibfk_3` FOREIGN KEY (`cod_emprestimo`) REFERENCES `emprestimo` (`cod_emprestimo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `emprestimo`
--
ALTER TABLE `emprestimo`
  ADD CONSTRAINT `emprestimo_ibfk_1` FOREIGN KEY (`ISBN_exemplar`) REFERENCES `exemplar` (`ISBN`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `emprestimo_ibfk_2` FOREIGN KEY (`codigo_assoc`) REFERENCES `associado` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `emprestimo_ibfk_3` FOREIGN KEY (`codigo_func`) REFERENCES `funcionario` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `emprestimo_ibfk_4` FOREIGN KEY (`num_exemplar`) REFERENCES `exemplar` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exemplar`
--
ALTER TABLE `exemplar`
  ADD CONSTRAINT `exemplar_ibfk_1` FOREIGN KEY (`ISBN`) REFERENCES `publicacao` (`ISBN`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `multa`
--
ALTER TABLE `multa`
  ADD CONSTRAINT `multa_ibfk_1` FOREIGN KEY (`cod_emprestimo`) REFERENCES `emprestimo` (`cod_emprestimo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
